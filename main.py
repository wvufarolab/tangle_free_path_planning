import pyvisgraph as vg
from pyvisgraph.visible_vertices import edge_distance
import matplotlib.pyplot as plt
from graph import DiGraph
from algorithms import dijkstra
import homotopy as h


def draw(polys, centers=[]):
	# Draw the obstacles
	for i in range(0, len(polys)):
		for j in range(0,len(polys[i])-1):
			plt.plot([polys[i][j].x, polys[i][j+1].x], [polys[i][j].y, polys[i][j+1].y], 'b')
		plt.plot([polys[i][0].x, polys[i][len(polys[i])-1].x], [polys[i][0].y, polys[i][len(polys[i])-1].y], 'b')
		if centers:	
			plt.plot(centers[i][0].x, centers[i][0].y, 'ko')
			plt.plot([centers[i][0].x, centers[i][0].x], [centers[i][0].y, centers[i][0].y+100], 'k:')
			plt.text(centers[i][0].x+0.3, centers[i][0].y, str(centers[i][1]))
	plt.axis('equal')
	plt.axis([-10, 10,-10, 10])
	plt.xlabel("x (m)")
	plt.ylabel("y (m)")
	


def main():

	
	# Create the obstacles
	polys = [[vg.Point(0.0,6.0), vg.Point(3.0,6.0), vg.Point(1.5,9.0)],
		 [vg.Point(2.0, 0.0), vg.Point(0.7071*2, 0.7071*2), vg.Point(0.0, 2.0), vg.Point(-0.7071*2, 0.7071*2), vg.Point(-2.0, 0.0), vg.Point(-0.7071*2, -0.7071*2), vg.Point(0.0, -2.0), vg.Point(0.7071*2, -0.7071*2)],	
		 [vg.Point(5.0,0.0), vg.Point(7.0,0.0), vg.Point(7.0,2.0), vg.Point(5.0,2.0)],
        	 [vg.Point(6.0,4.0), vg.Point(9.0,4.0), vg.Point(9.0,7.0), vg.Point(7.5,8.0), vg.Point(5.0,4.5)],
		 [vg.Point(-3.0*1.5,3.0*1.5), vg.Point(-6.0*1.5,2.0*1.5), vg.Point(-6.0*1.5,6.0*1.5), vg.Point(-2.5*1.5,6.0*1.5), vg.Point(-2.0*1.5,1.5*1.5)],
		 [vg.Point(-8.0,-4.0), vg.Point(-5.0,-4.0), vg.Point(-6.5,0.5)],
		 [vg.Point(3.0+5.0, -5.0), vg.Point(0.7071*3+5.0, 0.7071*3-5.0), vg.Point(0.0+5.0, 3.0-5.0), vg.Point(-0.7071*3+5.0, 0.7071*3-5.0), vg.Point(-3.0+5.0, -5.0), vg.Point(-0.7071*3+5.0, -0.7071*3-5.0), vg.Point(0.0+5.0, -3.0-5.0), vg.Point(0.7071*3+5.0, -0.7071*3-5.0)],
		 [vg.Point(9.1, 0.0), vg.Point(9.0, -1.0), vg.Point(9.8, -1.0)]]
	
	centers = h.findCenters(polys)

	plt.figure(2)
	draw(polys, centers)	
 

	# Create the visibility graph
	g = vg.VisGraph()  # This is the visibility graph
	g.build(polys)

	
        # Plot the visibility graph
	plt.figure(1)
	#draw(polys)	
	for i in range(0, len(polys)):
		for j in range(0,len(polys[i])):
			for edges in g.visgraph[polys[i][j]]:
				adj=edges.get_adjacent(polys[i][j])				
				plt.plot([polys[i][j].x, adj.x], [polys[i][j].y, adj.y], 'b:')
			plt.plot(polys[i][j].x, polys[i][j].y, 'ro', markersize=8)
	plt.axis('equal')
	plt.axis([-10, 10,-10, 10])
	plt.xlabel("x (m)")
	plt.ylabel("y (m)")
	plt.ginput()


	G = DiGraph()      # This is the graph for graph search
	node = 1;
	nodenum={}         # This is a dictionary that transform 2D positions in node labels in G (numbers)

	# Create the Graph with nodes only
	for i in range(0, len(polys)):
		for j in range(0,len(polys[i])):
			G.add_node(str(node))
			nodenum[polys[i][j]]=str(node)
			node=node+1

	# Create the edges with costs
	for i in range(0, len(polys)):
		for j in range(0,len(polys[i])):
			for edges in g.visgraph[polys[i][j]]:
				adj=edges.get_adjacent(polys[i][j])				
				cost = edge_distance(polys[i][j], adj)
				signature = h.Signature(centers, polys[i][j], adj)
				G.add_edge(nodenum[polys[i][j]], nodenum[adj], cost, signature)
				
	print node
	# Start is the anchor point
	#start=plt.ginput();
	start=[[-4.0,0.0]]	
	start_point = vg.Point(start[0][0], start[0][1])

	# Update the visibility map
	g.update([start_point], start_point)

	# Update the search graph with the start_point
	G.add_node(str(node))
	nodenum[start_point]=str(node)
	node=node+1
	for edges in g.visgraph[start_point]:
		adj=edges.get_adjacent(start_point)
		cost = edge_distance(start_point, adj)
		signature = h.Signature(centers, start_point, adj)
		G.add_edge(nodenum[start_point], nodenum[adj], cost, signature)
		G.add_edge(nodenum[adj], nodenum[start_point], cost, h.Invert(signature))	

	# Plot the base position
	plt.figure(2)
	plt.plot(start_point.x, start_point.y, 'r*', markersize=15)
	plt.text(start_point.x+0.4, start_point.y, r'$p_b$', fontsize=14)

	spath = {'cost': 0, 
          	 'path': [nodenum[start_point]]}
	hSigk = h.pathSignature(G, spath['path'])

	current_point = start_point
	short_path=[]
	
	saved_shortest_paths=[]
	accumulated_shortest_path_cost=0;
	saved_robot_paths=[]
	accumulated_robot_path_cost=0;

	###### Loop
	for cont in range(0,5):
	
		# Read goal
		while 1:
			goal=plt.ginput();
	        	goal_point = vg.Point(goal[0][0], goal[0][1])			
			if g.point_in_polygon(goal_point) == -1: break       
	 
		# Update the visibility map
		g.update([goal_point], start_point, goal_point)
		g.update([current_point], start_point, goal_point)
	
		# Update the search graph with the goal_point
		G.add_node(str(node))
		nodenum[goal_point]=str(node)
		node=node+1
		for edges in g.visgraph[goal_point]:
			adj=edges.get_adjacent(goal_point)
			cost = edge_distance(goal_point, adj)
			signature = h.Signature(centers, goal_point, adj)
			G.add_edge(nodenum[adj], nodenum[goal_point], cost, h.Invert(signature)) 
			G.add_edge(nodenum[goal_point], nodenum[adj], cost, signature) 

	        plt.clf()    
		draw(polys, centers)
		
		# Plot the base position
		plt.plot(start_point.x, start_point.y, 'r*', markersize=15)
		plt.text(start_point.x+0.4, start_point.y, r'$p_b$', fontsize=14)

		print " *********** New Motion *************** "

		# Plot the previous shortest tether and current point
		plt.plot(current_point.x, current_point.y, 'rs', markersize=8)
		plt.text(current_point.x+0.4, current_point.y, r'$p_c$', fontsize=14)		
                for j in range(0, len(short_path)-1):
			plt.plot([short_path[j].x, short_path[j+1].x] , [short_path[j].y, short_path[j+1].y], ':g', linewidth=1)
			

		hSigkant = hSigk
		previous_spath=spath
		

		# Compute the new shortest tether		
		spath = dijkstra(G, nodenum[start_point], nodenum[goal_point])
		hSigk =  h.pathSignature(G, spath['path'])

		# Plot the new shortest tether and new goal
		plt.plot(goal_point.x, goal_point.y, 'yo', markersize=10)
		plt.text(goal_point.x+0.4, goal_point.y, r'$p_g$', fontsize=14)	
		short_path=[] 
		for i in range(0, len(spath['path'])):	
			for point, value in nodenum.items(): 
	         		if spath['path'][i] == value:
					short_path.append(point)
		for j in range(0, len(short_path)-1):
			plt.plot([short_path[j].x, short_path[j+1].x] , [short_path[j].y, short_path[j+1].y], 'g', linewidth=1)
 

		# Compute the desired signature
		hSigStar = h.Reduce(h.Invert(hSigkant)+hSigk)
		
		# Compute the robot path
		k=20
		shorthpath = h.shp(G, previous_spath, nodenum[goal_point], k, hSigStar)
		
		#if found: 
		if shorthpath:
			_short_path=[] 
	        	print "Cost:%s\t%s" % (shorthpath['cost'], "->".join(shorthpath['path']))
			for i in range(0, len(shorthpath['path'])):		
				for point, value in nodenum.items(): 
	         			if shorthpath['path'][i] == value:
					 	_short_path.append(point)
			
			
			for j in range(0, len(_short_path)-1):
				plt.plot([_short_path[j].x, _short_path[j+1].x] , [_short_path[j].y, _short_path[j+1].y], 'r--', linewidth=2)

				
			# shortest path	
			shortest_local_path = dijkstra(G, previous_spath['path'][-1], nodenum[goal_point])
			temp_path=[] 
	       		for i in range(0, len(shortest_local_path['path'])):	
				for point, value in nodenum.items(): 
	         			if shortest_local_path['path'][i] == value:
						temp_path.append(point)
			for j in range(0, len(temp_path)-1):
				plt.plot([temp_path[j].x, temp_path[j+1].x] , [temp_path[j].y, temp_path[j+1].y], 'y', linewidth=2)

			saved_shortest_paths.insert(0, temp_path) # save the shortest path to show tangling
			accumulated_shortest_path_cost+=shortest_local_path['cost']

			saved_robot_paths.insert(0, _short_path) # save the robot path to show tangling free
			accumulated_robot_path_cost+=shorthpath['cost']
		else:
	
			print "Did not find a solution in ", k, " tries -> using simplified longest path."

			# Longest path
		
			longestPath = list(reversed(previous_spath['path']))+spath['path'][1:]
			print "Original:  ", longestPath
			simp_path = h.SimplifyPath(G, longestPath, hSigStar)
			print "Simplified:", simp_path['path']
			_simp_path = []
			for i in range(0, len(simp_path['path'])):		
				for point, value in nodenum.items(): 
	         			if simp_path['path'][i] == value:
					 	_simp_path.append(point)

			# shortest path	
			shortest_local_path = dijkstra(G, previous_spath['path'][-1], nodenum[goal_point])
			temp_path=[] 
	       		for i in range(0, len(shortest_local_path['path'])):	
				for point, value in nodenum.items(): 
	         			if shortest_local_path['path'][i] == value:
						temp_path.append(point)
			for j in range(0, len(temp_path)-1):
				plt.plot([temp_path[j].x, temp_path[j+1].x] , [temp_path[j].y, temp_path[j+1].y], 'y', linewidth=2)

			saved_shortest_paths.insert(0, temp_path) # save the shortest path to show tangling
			accumulated_shortest_path_cost+=shortest_local_path['cost']

			# Simplified longest path	
			for j in range(0, len(_simp_path)-1):
				plt.plot([_simp_path[j].x, _simp_path[j+1].x] , [_simp_path[j].y, _simp_path[j+1].y], 'k--', linewidth=2)
			
			saved_robot_paths.insert(-1, _simp_path) # save the robot path to show tangling free
			accumulated_robot_path_cost+=simp_path['cost']
		

		# Remove current point from the graph	
		if current_point != start_point:
			for edges in g.visgraph[current_point]: 
				adj=edges.get_adjacent(current_point)
				g.visgraph[adj].remove(vg.Edge(adj, current_point))
			g.visgraph[current_point].clear()
			for key in G._data[nodenum[current_point]].keys():
				G._data[key].pop(nodenum[current_point])
				G._hdata[key].pop(nodenum[current_point])
			G._data.pop(nodenum[current_point])
			G._hdata.pop(nodenum[current_point])	
			del nodenum[current_point]
			

		# Update current point	
		current_point = goal_point
		

	# Plot final tether configuration	
	plt.figure(3)
	draw(polys)
	plt.plot(start_point.x, start_point.y, 'r*', markersize=15)
	plt.text(start_point.x+0.4, start_point.y, '$p_b$', fontsize=14)
	i=len(saved_shortest_paths);
	for path in saved_shortest_paths:
		for j in range(0, len(path)-1):
				plt.plot([path[j].x, path[j+1].x] , [path[j].y, path[j+1].y], 'y--', linewidth=2)
		plt.plot(path[-1].x, path[-1].y, 'ro', markersize=8 )
		plt.text(path[-1].x+0.3, path[-1].y, '$p_{'+str(i)+'}$', fontsize=14)
		i-=1

	plt.figure(4)
	draw(polys)
	plt.plot(start_point.x, start_point.y, 'r*', markersize=15)
	plt.text(start_point.x+0.4, start_point.y, '$p_b$', fontsize=14)
	i=len(saved_robot_paths);
	for path in saved_robot_paths:
		for j in range(0, len(path)-1):
				plt.plot([path[j].x, path[j+1].x] , [path[j].y, path[j+1].y], 'y--', linewidth=2)
		plt.plot(path[-1].x, path[-1].y, 'ro', markersize=8 )
		plt.text(path[-1].x+0.3, path[-1].y, '$p_{'+str(i)+'}$', fontsize=14)
		i-=1
	
	print 'Total robot path cost:',	accumulated_robot_path_cost
	print 'Total shortest path cost:',	accumulated_shortest_path_cost
	
	plt.show()

	

if __name__ == "__main__":
    	main()

