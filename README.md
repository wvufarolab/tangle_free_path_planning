## Pyton code of the paper "Online planning of tangle-free paths for a tethered mobile robot"
### Author: Guilherme A. S. Pereira - West Virginia University



#### Prerequisites

* Internet connection
* Python 2
* pip
* Linux
* git

#### Install dependencies

``` bash
$ pip install tqdm
$ pip install pyvisgraph
$ pip install matplotlib
```

#### Clone the package

``` bash
$ git clone https://guilhermepereira@bitbucket.org/wvufarolab/tangle_free_path_planning.git
```

#### Run the code

``` bash
$ cd tangle_free_path_planning
$ python main.py
```




